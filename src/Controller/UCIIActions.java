package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.UCII;
import view.GraphPanel;
import view.TrianguloPotencia;

public class UCIIActions implements ActionListener {
	private JPanel UCIIPanel; JTextField ampTensao; JTextField angTensao; GraphPanel tensaoGraph2; JTextField ampCorrente; JTextField angCorrente; GraphPanel correnteGraph2; GraphPanel potInstGraph; JTextField potAtiva; JTextField potReativa; JTextField potAparente; JTextField fatPot; TrianguloPotencia triangPot;
	UCII tensaoGraph = new UCII();
	UCII correnteGraph = new UCII();
		
	public UCIIActions(JPanel UCIIPanel, JFrame startScreen, JTextField ampTensao, JTextField angTensao, GraphPanel tensaoGraph2, JTextField ampCorrente, JTextField angCorrente, GraphPanel correnteGraph2, GraphPanel potInstGraph, JTextField potAtiva, JTextField potReativa, JTextField potAparente, JTextField fatPot, TrianguloPotencia triangPot) throws IOException{
		this.UCIIPanel=UCIIPanel;
		this.ampTensao =ampTensao;
		this.ampCorrente = ampCorrente;
		this.angTensao = angTensao;
		this.angCorrente = angCorrente;
		this.tensaoGraph2 = tensaoGraph2;
		this.correnteGraph2 = correnteGraph2;
		this.potInstGraph = potInstGraph;
		this.potAtiva = potAtiva;
		this.potReativa = potReativa;
		this.potAparente = potAparente;
		this.fatPot = fatPot;
		this.triangPot =triangPot;
	}

	public void actionPerformed(ActionEvent e) {
		String comando = e.getActionCommand();
		if(comando.equals("ok")) {
			double amp_ten, ang_ten;
			
			amp_ten = Double.parseDouble(ampTensao.getText());
            ang_ten = Double.parseDouble(angTensao.getText());
            List<Double> lista = tensaoGraph.Grafico(amp_ten, ang_ten);
             
			GraphPanel tensao_graph = new GraphPanel(lista);
			tensao_graph.setBounds(273, 72, 407, 134);
	        tensaoGraph2.setVisible(false);
	        UCIIPanel.add(tensao_graph);
	        tensao_graph.revalidate();
	        tensao_graph.repaint();				
		}else if(comando.equals("ok2")) {
			double amp_cor, ang_cor;
			
			amp_cor = Double.parseDouble(ampCorrente.getText());
            ang_cor = Double.parseDouble(angCorrente.getText());
            List<Double> lista = correnteGraph.Grafico(amp_cor, ang_cor);
              
			GraphPanel corrente_graph = new GraphPanel(lista);
			corrente_graph.setBounds(273, 219, 407, 140);
	        correnteGraph2.setVisible(false);
	        UCIIPanel.add(corrente_graph);
	        corrente_graph.revalidate();
	        corrente_graph.repaint();
		}else if(comando.equals("ok3")) {
			double amp_cor, ang_cor,amp_ten, ang_ten;
			int valorPotAtiva = 0, valorPotReativa = 0;
	        
			amp_ten = Double.parseDouble(ampTensao.getText());
            ang_ten = Double.parseDouble(angTensao.getText());
			amp_cor = Double.parseDouble(ampCorrente.getText());
            ang_cor = Double.parseDouble(angCorrente.getText());
             
             
 			potInstGraph.setVisible(false); 
			List<Double> lista = null;
			GraphPanel potInstGraph = new GraphPanel(lista);
			potInstGraph.setBounds(393, 423, 650, 150);
			UCIIPanel.add(potInstGraph);
	        potInstGraph.revalidate();
			potInstGraph.repaint();
	        
			triangPot.setVisible(false);
			TrianguloPotencia triangPot = new TrianguloPotencia(valorPotAtiva,valorPotReativa);
			triangPot.setBounds(692, 72, 414,309);
			UCIIPanel.add(triangPot);
	        triangPot.revalidate();
	        triangPot.repaint();	
		}
	}	
}
