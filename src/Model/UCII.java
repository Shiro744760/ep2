package model;


import java.util.ArrayList;
import java.util.List;

public class UCII extends GeneralLogic {
	private double corrente;
	private double angCorrente;
	private double volt;
	private double angFase;
	private List<Double> lista = new ArrayList<>();
	private static float frequenciaAng = (float) (3.14*2*60);
	//private static float frequenciaAng1 = (float) (3.14*2*60);
	
	public void CorrenteTensao() {
		this.corrente = 0;
		this.angCorrente = -180;
	}
	
	public  List<Double> Grafico(double corrente, double angCorrente){
		setLista(lista);
		setAmp(corrente);
		setAng(angCorrente);
		
		for (float i = -100; i < 100; i = (float) (i + 0.4)) {
			getLista().add(getAmp()*(Math.cos(frequenciaAng*i + getAng())));
        }
		return getLista();
	}

	public void CorrenteTensao1() {
		this.volt = 0;
		this.angFase = -180;
	}
	
	public  List<Double>  Grafico1(double volt, double angTensao){
		List<Double> lista = new ArrayList<>();
		setAmp(volt);
		setAng(angTensao);
		
		for (float i = -50; i < 50; i = (float) (i + 0.4)) {
			lista.add(getAmp()*(Math.cos(frequenciaAng*i + getAng())));
        }
		return lista;		
	}

	public List<Double> GraficoTensao(double ampTensao, double angTensao) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Double> GraficoCorrente(double ampCorrente, double angCorrente) {
		// TODO Auto-generated method stub
		return null;
	}
}

