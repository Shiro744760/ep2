package view;

import java.io.IOException;

import javax.swing.JFrame;

public class Main {
	public static void main(String [] args) throws IOException{
		JFrame startScreen = new JFrame();
		startScreen.setSize(1133, 650);
		new MainWindow(startScreen);
	}
}
