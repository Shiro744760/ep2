package view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.UCIIActions;
import java.awt.Font;
import java.awt.Color;

public class UCII {
	private JPanel UserCaseIIPanel; GraphPanel tensaoGraph; GraphPanel correnteGraph; GraphPanel potInstGraph; TrianguloPotencia triangPot; JTextField ampTensao; JTextField angTensao; JTextField ampCorrente; JTextField angCorrente; JTextField potAtiva; JTextField potReativa; JTextField potAparente; JTextField fatPot; List<Double> valorTensao = new ArrayList<>(); List<Double> valorCorrente = new ArrayList<>(); List<Double> valorPotInst = new ArrayList<>();

	public UCII(JFrame startScreen) throws IOException {		
		generateUserCaseIIPanel(startScreen);
	}
	
	private void generateUserCaseIIPanel(JFrame startScreen) throws IOException {
		UserCaseIIPanel = new JPanel();
		UserCaseIIPanel.setLayout(null);
		
		JLabel blocoMain = new JLabel("Fluxo de Pot�ncia Fundamental:");
		blocoMain.setBounds(500, 0, 350, 40);
		blocoMain.setBackground(Color.WHITE);
		blocoMain.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 13));
		UserCaseIIPanel.add(blocoMain);
		
		JLabel input = new JLabel("Inserir Dados:");
		input.setBounds(25, 40, 170, 40);
		input.setFont(new Font("Times New Roman", Font.BOLD, 13));
		UserCaseIIPanel.add(input);
		
		JLabel output = new JLabel("Resultados -->");
		output.setBounds(25, 390, 116, 20);
		output.setFont(new Font("Times New Roman", Font.BOLD, 13));
		UserCaseIIPanel.add(output);
		
		ampTensao = new JTextField();
		ampTensao.setBounds(12, 50, 120, 20);
		ampTensao.setColumns(10);
		
		angTensao = new JTextField();
		angTensao.setBounds(12, 107, 120, 20);
		angTensao.setColumns(10);
		
		JButton ok3 = new JButton("GO");
		ok3.setBounds(153, 390, 70, 20);
		UserCaseIIPanel.add(ok3);
		ok3.setActionCommand("ok3");
		
		correnteGraph = new GraphPanel(valorCorrente);
		correnteGraph.setBounds(273, 219, 407, 140);
		
		potInstGraph = new GraphPanel(valorPotInst);
		potInstGraph.setBounds(393, 423, 650, 150);
		
		triangPot = new TrianguloPotencia(0,0);
		triangPot.setBounds(692, 72, 414, 309);
		
		UserCaseIIPanel.add(correnteGraph);
		UserCaseIIPanel.add(potInstGraph);
		UserCaseIIPanel.add(triangPot);
		
		JLabel amp_tensaoLabel = new JLabel("Amplitude Tens�o:");
		amp_tensaoLabel.setBounds(12, 23, 120, 20);
		
		JLabel ang_tensaoLabel = new JLabel("Angulo de fase: ");
		ang_tensaoLabel.setBounds(12, 74, 97, 20);
		
		JPanel tensao_panel = new JPanel();
		tensao_panel.setBounds(17, 72, 224, 140);
		tensao_panel.setLayout(null);
		UserCaseIIPanel.add(tensao_panel);
	    tensao_panel.add(ampTensao);
	    tensao_panel.add(angTensao);
	    tensao_panel.add(amp_tensaoLabel);
	    tensao_panel.add(ang_tensaoLabel);
	    
	    JButton ok = new JButton("GO");
		ok.setBounds(142, 107, 70, 20);
		tensao_panel.add(ok);
		ok.setActionCommand("ok");
	    
	    JLabel pot_ativaLabel = new JLabel("Pot�ncia Ativa: ");
	    pot_ativaLabel.setBounds(17, 423, 170, 40);
		UserCaseIIPanel.add(pot_ativaLabel);
		
		potAtiva = new JTextField();
		potAtiva.setBounds(186, 433, 140, 20);
		potAtiva.setColumns(10);
		UserCaseIIPanel.add(potAtiva);
		
		JLabel pot_reativaLabel = new JLabel("Pot�ncia Reativa:");
		pot_reativaLabel.setBounds(17, 457, 170, 40);
		UserCaseIIPanel.add(pot_reativaLabel);
		
		potReativa = new JTextField();
		potReativa.setBounds(186, 467, 140, 20);
		potReativa.setColumns(10);
		UserCaseIIPanel.add(potReativa);
		
		JLabel pot_aparenteLabel = new JLabel("Pot�ncia Aparente: ");
		pot_aparenteLabel.setBounds(17, 495, 170, 40);
		UserCaseIIPanel.add(pot_aparenteLabel);
		
		potAparente = new JTextField();
		potAparente.setBounds(186, 505, 140, 20);
		potAparente.setColumns(10);
		UserCaseIIPanel.add(potAparente);
		
		JLabel fat_potLabel = new JLabel("Fator de Pot�ncia: ");
		fat_potLabel.setBounds(17, 534, 170, 40);
		UserCaseIIPanel.add(fat_potLabel);
		
		fatPot = new JTextField();
		fatPot.setBounds(186, 544, 140, 20);
		fatPot.setColumns(10);
		UserCaseIIPanel.add(fatPot);
		
		correnteGraph.setLayout(null);
		
		startScreen.getContentPane().add(UserCaseIIPanel);
		
		tensaoGraph = new GraphPanel(valorTensao);
		tensaoGraph.setBounds(273, 72, 407, 134);
		UserCaseIIPanel.add(tensaoGraph);
		tensaoGraph.setLayout(null);
		
		ampCorrente = new JTextField();
		ampCorrente.setBounds(12, 50, 122, 20);
		ampCorrente.setColumns(10);
		
		angCorrente = new JTextField();
		angCorrente.setBounds(12, 107, 122, 20);
		angCorrente.setColumns(10);
		
		JLabel amp_correnteLabel = new JLabel("Amplitude da Corrente:");
		amp_correnteLabel.setBounds(12, 26, 137, 20);
		
		JLabel ang_correnteLabel = new JLabel("Angulo de Fase: ");
		ang_correnteLabel.setBounds(12, 83, 97, 20);
			    
	    JPanel corrente_panel = new JPanel();
	    corrente_panel.setBounds(17, 217, 224, 140);
	    UserCaseIIPanel.add(corrente_panel);
	    corrente_panel.setLayout(null);
	    corrente_panel.add(ampCorrente);
	    corrente_panel.add(angCorrente);
	    corrente_panel.add(amp_correnteLabel);
	    corrente_panel.add(ang_correnteLabel);
	    
	    JButton ok2 = new JButton("GO");
	    ok2.setBounds(143, 107, 70, 20);
	    ok2.setActionCommand("ok2");
	    corrente_panel.add(ok2);
	    	    
	    ok.addActionListener(new UCIIActions(UserCaseIIPanel,startScreen,ampTensao,angTensao, tensaoGraph, ampCorrente, angCorrente, correnteGraph,potInstGraph,potAtiva,potReativa,potAparente,fatPot,triangPot));
	    ok2.addActionListener(new UCIIActions(UserCaseIIPanel,startScreen,ampTensao,angTensao, tensaoGraph,ampCorrente,angCorrente, correnteGraph,potInstGraph,potAtiva,potReativa,potAparente,fatPot,triangPot));
	    ok3.addActionListener(new UCIIActions(UserCaseIIPanel,startScreen,ampTensao,angTensao, tensaoGraph	    	    ,ampCorrente,angCorrente, correnteGraph,potInstGraph,potAtiva,potReativa,potAparente,fatPot,triangPot));
	    
	    UserCaseIIPanel.setVisible(true);
	}
}
