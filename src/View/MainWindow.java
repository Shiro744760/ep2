package view;

import java.awt.Font;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import controller.MainActions;

public class MainWindow extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel mainPanel;
	
public MainWindow(JFrame startScreen) throws IOException{
	GenerateMW(startScreen);		
	}

	public void GenerateMW(JFrame startScreen) throws IOException{
		JButton UCII = new JButton("Fluxo de Pot�ncia Fundamental");
		UCII.setBounds(333, 247, 358, 30);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1330, 750);
		mainPanel = new JPanel();
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(mainPanel);
		
		UCII.setActionCommand("sim1");
		UCII.addActionListener(new MainActions(mainPanel,startScreen));;;
		startScreen.getContentPane().add(mainPanel);
		mainPanel.setLayout(null);
		mainPanel.add(UCII);
		
		JLabel lblAprendaQee = new JLabel("Aprenda QEE");
		lblAprendaQee.setBounds(424, 61, 162, 58);
		lblAprendaQee.setFont(new Font("Times New Roman", Font.BOLD, 25));
		mainPanel.add(lblAprendaQee);

		JLabel lblBemVindoAo = new JLabel("Bem vindo ao programa Aprenda QEE. Comece sua simula��o: ");
		lblBemVindoAo.setBounds(292, 154, 670, 16);
		lblBemVindoAo.setFont(new Font("Times New Roman", Font.PLAIN, 17));
		mainPanel.add(lblBemVindoAo);
		
		startScreen.setVisible(true);
	}
}

